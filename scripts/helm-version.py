#!/usr/bin/env python3
import sys
import argparse
import semver
import logging
import subprocess
import json


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="This is my super sweet script")
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    section_choices = ["major", "minor", "patch"]
    parser.add_argument(
        "-s",
        "--section",
        help="Section of version to bump",
        choices=section_choices,
        default="patch",
    )
    parser.add_argument("chart", help="Name of chart to look up")

    return parser.parse_args()


def find_current_version(chart):
    info_json = subprocess.check_output(
        ["helm", "search", "repo", "-r", "\v%s\v" % chart, "-l", "-o", "json"]
    )
    info = json.loads(info_json)
    if len(info) == 0:
        logging.info(
            f"Could not find any releases for {chart}, assuming this is the first one"
        )
        v = "0.0.0"
    else:
        v = info[0]["version"]
    return v


def main():
    args = parse_args()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    current_v = semver.VersionInfo.parse(find_current_version(args.chart))
    logging.info("Found current version: %s" % current_v)

    if args.section == "patch":
        logging.info("Bumping patch level")
        current_v = current_v.bump_patch()
    elif args.section == "minor":
        logging.info("Bumping minor level")
        current_v = current_v.bump_minor()
    elif args.section == "major":
        logging.info("Bumping major level")
        current_v = current_v.bump_major()
    print(current_v)

    return 0


if __name__ == "__main__":
    sys.exit(main())
