from ubuntu:20.04

ENV VAULT_ADDR https://vault-systems.oit.duke.edu
ENV DEBIAN_FRONTEND 'noninteractive'

RUN mkdir /code
COPY . /code/
WORKDIR /code

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

# Note there is a bug in 3.3.2 that busts repos, hard coding to 3.3.1 for now

# hadolint ignore=DL3008,DL3005
RUN apt-get -y update && \
    apt-get -y dist-upgrade && \
    apt-get -y install --no-install-recommends \
      curl ca-certificates gnupg2 python3-pip git software-properties-common && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh --version v3.3.1 && \
    apt-get -y update && \
    apt-get -y install --no-install-recommends vault && \
    rm -rf /var/lib/apt/lists/* && \
    helm plugin install https://github.com/chartmuseum/helm-push.git && \
    pip3 install -r ./requirements.txt
